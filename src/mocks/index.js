import poems from './data'
import firebase from 'firebase'
import users from './users';

export function saveEventsToFB() {
  const eventsRef = firebase.database().ref('/poems')
  poems.forEach(poem => eventsRef.push(poem))
}

window.runMigration = function () {
  firebase.database().ref('/poems').once('value', data => {
    if (!data.val()) saveEventsToFB()
  })
};

export function saveUsersToFB() {
  const eventsRef = firebase.database().ref('/users')
  users.forEach(user => eventsRef.push(user))
}

window.userMigration = function () {
  firebase.database().ref('/users').once('value', data => {
    if (!data.val()) saveUsersToFB()
  })
};