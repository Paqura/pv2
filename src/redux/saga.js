import { all } from 'redux-saga/effects';
import {saga as poemsSaga} from '../ducks/poems/saga';
import {saga as authSaga} from '../ducks/auth/saga';

export const saga = function * () {
  yield all([
    poemsSaga()
  ])
};
