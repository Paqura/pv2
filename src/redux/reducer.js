import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';
import { reducer as form } from 'redux-form';
import {reducer as poemsReducer, moduleName as poems} from '../ducks/poems';
import {reducer as authReducer, moduleName as auth} from '../ducks/auth';

export default combineReducers({
  router,
  form,
  [poems]: poemsReducer,
  [auth]: authReducer
})