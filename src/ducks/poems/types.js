import {appName} from "../../config";
import { moduleName } from './index';

const prefix = `${appName}/${moduleName}`;

export const FETCH_POEMS_REQUEST = `${prefix}/FETCH_POEMS_REQUEST`;
export const FETCH_POEMS_SUCCESS = `${prefix}/FETCH_POEMS_SUCCESS`;
export const FETCH_POEMS_FAILURE = `${prefix}/FETCH_POEMS_FAILURE`;

export const FETCH_LAZY_REQUEST = `${prefix}/FETCH_LAZY_REQUEST`;
export const FETCH_LAZY_SUCCESS = `${prefix}/FETCH_LAZY_SUCCESS`;
export const FETCH_LAZY_FAILURE = `${prefix}/FETCH_LAZY_FAILURE`;