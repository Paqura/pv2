import {call, select, take, put, all} from "redux-saga/effects";
import {database} from "firebase";
import {stateSelector} from './selectors';
import {
  FETCH_POEMS_FAILURE,
  FETCH_POEMS_REQUEST,
  FETCH_POEMS_SUCCESS,
  FETCH_LAZY_FAILURE,
  FETCH_LAZY_REQUEST,
  FETCH_LAZY_SUCCESS
} from "./types";

export const fetchPoemsSaga = function* () {
  while (true) {
    yield take(FETCH_POEMS_REQUEST);
    try {
      const ref = database().ref('poems')
          .orderByKey()
          .limitToFirst(10);

      const data = yield call([ref, ref.once], 'value');
      yield put({
        type: FETCH_POEMS_SUCCESS,
        payload: data.val()
      })
    } catch (error) {
      yield put({
        type: FETCH_POEMS_FAILURE,
        error
      })
    }
  }
};

export const fetchLazySaga = function* () {
  while (true) {
    yield take(FETCH_LAZY_REQUEST);
    try {
      const state = yield select(stateSelector);
      const last = state.entities.last();
      const ref = database().ref('poems')
          .orderByKey()
          .limitToFirst(10)
          .startAt(last ? last.uid : '');

      const data = yield call([ref, ref.once], 'value');
      yield put({
        type: FETCH_LAZY_SUCCESS,
        payload: data.val()
      })
    } catch (error) {
      yield put({
        type: FETCH_LAZY_FAILURE,
        error
      })
    }
  }
};

export function* saga() {
  yield all([
    fetchPoemsSaga(),
    fetchLazySaga()
  ])
}