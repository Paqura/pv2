import {Record, OrderedMap} from 'immutable';
import {fbDataToEntities} from '../../utils';
import {
  FETCH_POEMS_SUCCESS,
  FETCH_POEMS_REQUEST,
  FETCH_LAZY_REQUEST,
  FETCH_LAZY_SUCCESS
} from './types';
import {ADD_TO_FAVORITE_SUCCESS} from "../favorites/types";

export const moduleName = 'poems';

export const ReducerState = Record({
  entities: new OrderedMap({}),
  loading: false,
  lazy: false,
  loaded: false
});

export const PoemsShema = Record({
  uid: null,
  text: null,
  title: null,
  picture: null,
  added: false
});

export function reducer(state = new ReducerState(), action) {
  const {type, payload} = action;
  switch (type) {
    case FETCH_POEMS_REQUEST:
      return state
          .set('loading', true);
    case FETCH_LAZY_REQUEST:
      return state
          .set('lazy', true);
    case FETCH_LAZY_SUCCESS:
    case FETCH_POEMS_SUCCESS:
      return state
          .set('loading', false)
          .set('lazy', true)
          .mergeIn(['entities'], fbDataToEntities(payload, PoemsShema))
          .set('loaded', Object.keys(payload).length < 10);
    default:
      return state;
  }
}
