import { FETCH_POEMS_REQUEST, FETCH_LAZY_REQUEST } from './types';

export function fetchPoems() {
  return {
    type: FETCH_POEMS_REQUEST
  }
}

export function fetchLazyPoems() {
  return {
    type: FETCH_LAZY_REQUEST
  }
}