import {createSelector} from "reselect";
import {moduleName} from './index';

export const stateSelector = state => state[moduleName];
export const entitiesSelector = createSelector(stateSelector, state => state.entities);
export const poemsListSelector = createSelector(entitiesSelector, entities => (
    entities.valueSeq().toArray()
));

export const favoriteListSelector = state => {
  const arr = [];
  if(!state.auth.user) {
    return false;
  }
  let fav = state.auth.user.favorites;

  for (let key in fav) {
    if (fav.hasOwnProperty(key)) {
      arr.push(fav[key]);
    } else {
      return arr;
    }
  }
  return arr;
};