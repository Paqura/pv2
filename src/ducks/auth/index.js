import {Record} from "immutable";
import {usersIDS} from './selectors';
import {
  SIGN_UP_SUCCESS,
  FETCH_USERS_SUCCESS,
  SIGN_IN_SUCCESS,
  GET_USER_SESSION_SUCCESS
} from "./types";

import {
  ADD_TO_FAVORITE_FAILURE,
  ADD_TO_FAVORITE_SUCCESS,
  DELETE_FAVORITE_SUCCESS,
  FETCH_FAVORITE_REQUEST,
  FETCH_FAVORITE_SUCCESS
} from "../favorites/types";

const ReducerShema = Record({
  user: {
    id: null,
    name: null,
    lastName: null,
    favorites: []
  },
  userID: null,
  error: null,
  loading: false,
  users: [],
  ids: [],
  connected: false,
  adding: null,
  addedFailure: null
});

export const moduleName = "auth";

export function reducer(state = new ReducerShema(), action) {
  const {type, payload, added} = action;
  switch (type) {
    case FETCH_FAVORITE_REQUEST:
      return state
          .set('loading', true);
    case FETCH_FAVORITE_SUCCESS:
      const newUserState = state.users[state.user.id];
      return state
          .set('users', payload)
          .set('user', newUserState)
          .set('loading', false);
    case FETCH_USERS_SUCCESS:
      let ids = usersIDS(payload);
      return state
          .set('users', payload)
          .set('ids', ids);
    case SIGN_IN_SUCCESS:
      return state
          .set('user', state.users[payload]);
    case SIGN_UP_SUCCESS:
      return state
          .set('loading', false)
          .set('user', payload);
    case GET_USER_SESSION_SUCCESS:
      return state
          .set('connected', true);
    case ADD_TO_FAVORITE_SUCCESS:
      const newFavoriteList = Object.assign({}, state.user, state.user.favorites, payload);
      return state
          .set('adding', added)
          .set('user', newFavoriteList);
    case ADD_TO_FAVORITE_FAILURE:
      return state
          .set('addedFailure', payload);
    case DELETE_FAVORITE_SUCCESS:
      const f = delete state.user.favorites[Object.keys(state.user.favorites)];
      const updateFavoriteList = Object.assign({}, state.user, f);
      return state
          .set('user', updateFavoriteList);
    default:
      return state;
  }
}


