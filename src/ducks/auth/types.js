import {moduleName} from "./index";
import {appName} from "../../config";

const prefix = `${appName}/${moduleName}`;

export const SIGN_IN_REQUEST = `${prefix}/SIGN_IN_REQUEST`;
export const SIGN_IN_SUCCESS = `${prefix}/SIGN_IN_SUCCESS`;
export const SIGN_IN_FAILURE = `${prefix}/SIGN_IN_FAILURE`;

export const SIGN_UP_REQUEST = `${prefix}/SIGN_UP_REQUEST`;
export const SIGN_UP_SUCCESS = `${prefix}/SIGN_UP_SUCCESS`;
export const SIGN_UP_FAILURE = `${prefix}/SIGN_UP_FAILURE`;

export const FETCH_USERS_REQUEST = `${prefix}/FETCH_USERS_REQUEST`;
export const FETCH_USERS_SUCCESS = `${prefix}/FETCH_USERS_SUCCESS`;
export const FETCH_USERS_FAILURE = `${prefix}/FETCH_USERS_FAILURE`;

export const GET_USER_SESSION_REQUEST = `${prefix}/GET_USER_SESSION_REQUEST`;
export const GET_USER_SESSION_SUCCESS = `${prefix}/GET_USER_SESSION_SUCCESS`;