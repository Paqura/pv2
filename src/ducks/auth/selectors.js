export const usersIDS = data => {
  const result = [];
  for (let key in data) {
    result.push(data[key].id);
  }
  return result;
};

export const findSameID = (users, user) => {
  return users.filter(it => it === user.id);
};


