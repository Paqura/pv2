import {
  SIGN_IN_REQUEST,
  FETCH_USERS_REQUEST,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_FAILURE,
  SIGN_IN_SUCCESS,
  SIGN_UP_SUCCESS,
  GET_USER_SESSION_REQUEST,
  GET_USER_SESSION_SUCCESS
} from './types';

import {findSameID} from './selectors';

import firebase from 'firebase';

export const fetchUsers = () => async dispatch => {
  let users = await firebase.database().ref('users').once('value').then(res => res.val());

  dispatch({
    type: FETCH_USERS_REQUEST
  });

  if (typeof users === 'object') {
    dispatch({
      type: FETCH_USERS_SUCCESS,
      payload: users
    })
  } else {
    dispatch({
      type: FETCH_USERS_FAILURE
    })
  }
};

export const signUpRequest = users => async dispatch => {
  dispatch({
    type: SIGN_IN_REQUEST
  });

  // let user = {
  //   name: "AAAIVA2aN",
  //   lastName: "AST",
  //   id: 123130560,
  //   favorites: []
  // };

  function writeUserData(id, name, lastName, favorites) {
    firebase.database().ref('users/' + id).set({
      name,
      lastName,
      id,
      favorites
    });
  }

  // let sameID = findSameID(users, user);
  //
  // if (sameID.length === 1) {
  //   dispatch({
  //     type: SIGN_IN_SUCCESS,
  //     payload: sameID[0]
  //   })
  // } else {
  //   writeUserData(user.id, user.name, user.lastName, user.favorites);
  //   dispatch({
  //     type: SIGN_UP_SUCCESS,
  //     payload: user
  //   })
  // }


  VK.Auth.login((response) => {
    if (response.session) {
      let user = {};

      user.name = response.session.user.first_name;
      user.lastName = response.session.user.last_name;
      user.id = response.session.user.id;
      user.favorites = [];

      let sameID = findSameID(users, user);

      if (sameID.length === 1) {
        dispatch({
          type: SIGN_IN_SUCCESS,
          payload: sameID[0]
        })
      } else {
        writeUserData(user.id, user.name, user.lastName, user.favorites);
        dispatch({
          type: SIGN_UP_SUCCESS,
          payload: user
        })
      }
    } else {
      dispatch({
        type: SIGN_UP_FAILURE
      })
    }
  })
};

export const getUserSession = () => async dispatch => {
  dispatch({
    type: GET_USER_SESSION_REQUEST
  });
  // dispatch({
  //   type: GET_USER_SESSION_SUCCESS
  // })

  VK.Auth.getLoginStatus(function(response) {
    if (response.session) {
       dispatch({
         type: GET_USER_SESSION_SUCCESS
       })
    } else {
      return false;
    }
  })
};