
export const sameID = (id, favorites) => {
  let keys = Object.values(favorites);
  let ids = keys.map(it => it.id);
  let flag = false;
  for(let i = 0; i < ids.length; i++) {
    if(ids[i] === id) flag = true;
  }
  return flag;
};

