import {moduleName} from "./index";
import {appName} from "../../config";

const prefix = `${appName}/${moduleName}`;

export const ADD_TO_FAVORITE_REQUEST = `${prefix}/ADD_TO_FAVORITE_REQUEST`;
export const ADD_TO_FAVORITE_SUCCESS = `${prefix}/ADD_TO_FAVORITE_SUCCESS`;
export const ADD_TO_FAVORITE_FAILURE = `${prefix}/ADD_TO_FAVORITE_FAILURE`;

export const FETCH_FAVORITE_REQUEST = `${prefix}/FETCH_FAVORITE_REQUEST`;
export const FETCH_FAVORITE_SUCCESS = `${prefix}/FETCH_FAVORITE_SUCCESS`;

export const DELETE_FAVORITE_REQUEST = `${prefix}/DELETE_FAVORITE_REQUEST`;
export const DELETE_FAVORITE_SUCCESS = `${prefix}/DELETE_FAVORITE_SUCCESS`;
