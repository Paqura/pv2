import {
  ADD_TO_FAVORITE_REQUEST,
  ADD_TO_FAVORITE_SUCCESS,
  ADD_TO_FAVORITE_FAILURE,
  FETCH_FAVORITE_REQUEST,
  FETCH_FAVORITE_SUCCESS,
  DELETE_FAVORITE_REQUEST,
  DELETE_FAVORITE_SUCCESS
} from "./types";
import { sameID } from './selectors';
import firebase from 'firebase';
import history from '../../history';

export const addTOFavorite = (uid, userID) => async dispatch => {
  dispatch({
    type: ADD_TO_FAVORITE_REQUEST,
    payload: uid
  });

  let favorites =  await firebase.database().ref('users/' + userID + '/favorites').once('value');

  let flag = sameID(uid, favorites.val() || []);

  if(flag) {
    dispatch({
      type: ADD_TO_FAVORITE_FAILURE,
      payload: 'Уже добавлено'
    });
    return;
  }

  const favorite = await firebase.database().ref(`poems/${uid}`).once('value');

  if (favorite.val() !== null) {
    function writeNewFavorite(params) {
      let newPostKey = firebase.database().ref().child('users').push().key;

      let favoriteData = {
        id: uid,
        key: newPostKey,
        text: params.text,
        title: params.title,
        picture: params.picture
      };

      const updates = {};
      updates['users/' + userID + '/favorites/' + newPostKey] = favoriteData;

      return firebase.database().ref().update(updates);
    }

    writeNewFavorite(favorite.val());
    dispatch({
      type: ADD_TO_FAVORITE_SUCCESS,
      payload: favorite.val(),
      added: uid
    });
  } else {
    dispatch({
      type: ADD_TO_FAVORITE_FAILURE,
      payload: new Error('Пришёл null')
    });
  }
};

export const fetchFavorite = () => async dispatch => {
  dispatch({
    type: FETCH_FAVORITE_REQUEST
  });

  const favorites = await firebase.database().ref('users').once('value');

  dispatch({
    type: FETCH_FAVORITE_SUCCESS,
    payload: favorites.val()
  });
};

export const deleteFavoritePoem = (id, userID) => async dispatch => {
  dispatch({
    type: DELETE_FAVORITE_REQUEST
  });

  firebase.database().ref('users/' + userID + '/favorites/').child(id).remove();

  dispatch({
    type: DELETE_FAVORITE_SUCCESS,
    payload: id
  });
};