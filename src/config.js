import firebase from 'firebase';

export const appName = "poems-a4d62";

export const firebaseConfig = {
  apiKey: "AIzaSyAffVHrU6TH8R3mF_RK7n4COQ_25cwNP_o",
  authDomain: `${appName}.firebaseapp.com`,
  databaseURL: `https://${appName}.firebaseio.com`,
  projectId: appName,
  storageBucket: `${appName}.appspot.com`,
  messagingSenderId: "900850392695"
};

firebase.initializeApp(firebaseConfig);