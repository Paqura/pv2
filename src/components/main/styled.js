import styled from 'styled-components';

export const TypedText = styled.span`
   font-size: 16px;
   color: #262626;
   
`;

export const TypedWrapper = styled.article`
  position: absolute;
  top: 30%;
  left: 50%;
  transform: translateX(-50%);
  padding: 32px 8px;
  display: flex;
  width: 100%;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

export const Quote = styled.blockquote`
  font-weight: 600;
  font-size: 16px;
  border-left: 2px solid #3f51b5;
  padding-left: 16px;
  color: #262626;
`;