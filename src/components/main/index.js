import React, {Component} from 'react';
import Typist from 'react-typist';
import {TypedWrapper, TypedText, Quote} from './styled';

export default class CommonPage extends Component {

  render() {
    return (
        <Typist
            cursor={{show: false}}
        >
          <TypedWrapper>
            <TypedText>❝ Остановись, мгновенье! Ты не столь</TypedText>
            <TypedText>Прекрасно, сколько ты неповторимо. ❞</TypedText>
            <Typist.Delay ms={500}/>
            <Quote>И.А Бродский</Quote>
          </TypedWrapper>
        </Typist>
    );
  }
}