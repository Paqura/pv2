import React from 'react';
import ScrollToTop from 'react-scroll-up';
import Button from 'material-ui/Button';
import Icon from 'material-ui/Icon';
import { withStyles } from 'material-ui/styles';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  }
});

const buttonStyle = {
  "top" : "0",
  "left": "50%",
  "transform": "translateX(-50%)",
  "display": "flex",
  "justifyContent": "center",
  "alignItems": "flex-start",
  "height": "50px"
};

function ScrollUp(props) {
  const {classes} = props;
    return (
        <ScrollToTop showUnder={660} style={buttonStyle}>
          <Button className={classes.button} variant="raised" color="primary" style={{padding: "0"}}>
            <Icon>keyboard_arrow_up</Icon>
          </Button>
        </ScrollToTop>
    )
}

export default withStyles(styles)(ScrollUp);