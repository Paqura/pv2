import React from 'react';
import PropTypes from 'prop-types';

import {withStyles} from 'material-ui/styles';
import BottomNavigation, {BottomNavigationAction} from 'material-ui/BottomNavigation';
import RestoreIcon from 'material-ui-icons/Domain';
import FavoriteIcon from 'material-ui-icons/FavoriteBorder';
import SettingsIcon from 'material-ui-icons/Apps';
import LocationOnIcon from 'material-ui-icons/Storage';
import history from '../../../history';

const styles = {
  root: {
    width: 500,
  },
};

const navStyle = {
  'width': '100%',
  'position': 'sticky',
  'left': '0',
  'bottom': '0',
  'backgroundColor': '#f8f8f8',
  'boxShadow': '1px -4px 20px 0 rgba(0,0,0,.3)',
  'height': '48px'
};

const buttonStyle = {
  'padding': '10px'
}

class LabelBottomNavigation extends React.Component {
  state = {
    value: 'recents',
  };

  handleChange = (event, value) => {
    this.setState({value});
  };

  redirectHome() {
    history.push('/')
  }

  redirectPoems() {
    history.push('/poems')
  }

  redirectFavorite() {
    history.push('/favorite')
  }

  redirectMenu() {
    history.push('/settings')
  }

  currentValue() {
    return history.location.pathname.toString();
  }

  render() {
    const {classes} = this.props;
    return (
        <BottomNavigation
          value={this.currentValue()}
          onChange={this.handleChange}
          className={classes.root}
          style={navStyle}>
          <BottomNavigationAction
            className={classes.selected}
            onClick={this.redirectHome}
            value="/"
            icon={<RestoreIcon/>}
            style={buttonStyle}

          />
          <BottomNavigationAction
            onClick={this.redirectPoems}
            value="/poems"
            icon={<LocationOnIcon/>}
            style={buttonStyle}
          />
          <BottomNavigationAction
            onClick={this.redirectFavorite}
            value="/favorite"
            icon={<FavoriteIcon/>}
            style={buttonStyle}
          />
          <BottomNavigationAction
              onClick={this.redirectMenu}
              value="/settings"
              icon={<SettingsIcon/>}
              style={buttonStyle}
          />
        </BottomNavigation>
    );
  }
}

LabelBottomNavigation.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LabelBottomNavigation);