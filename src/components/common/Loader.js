// import React from "react";
// import { PoemPicture  } from '../poems/styled';
//
// const style = {
//   "width" : "100%",
//   "height" : "80vh",
//   "background": "#fff",
//   "display": "flex",
//   "alignItems": "center",
//   "justifyContent": "center"
// };
//
// function Loader() {
//   return  (
//     <div style={style}>
//       <span>
//         <PoemPicture src="./2.gif" alt="Loader..."/>
//       </span>
//     </div>
//   )
// }
//
// export default Loader;

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import { CircularProgress } from 'material-ui/Progress';

const styles = theme => ({
  progress: {
    margin: theme.spacing.unit * 2,
  },
});

const loaderStyle = {
  "width" : "100%",
  "height" : "80vh",
  "background": "#fff",
  "display": "flex",
  "alignItems": "center",
  "justifyContent": "center"
};

function Loader(props) {
  const { classes } = props;
  return (
      <div style={loaderStyle}>
        <CircularProgress className={classes.progress} color="primary" size={100}/>
      </div>
  );
}

Loader.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Loader);
