import styled from "styled-components";

export const styles = {
  root: {
    flexGrow: 1,
  },
  flex: {
    flex: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
};

export const NavBarTitle = styled.span`
   font-size: 16px;
   letter-spacing: 1px;
`;

export const singInStyle = {
  "fontSize": "14px",
  "letterSpacing": "0.4px",
  "textTransform": "capitalize",
  "color": "#e6e5e5",
  "display": "flex",
  "justifyContent": "flex-end",
  "padding": "0"
};
