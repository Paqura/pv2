import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import Icon from 'material-ui/Icon';
import {connect} from 'react-redux';
import {moduleName} from "../../../ducks/auth";
import {signUpRequest} from "../../../ducks/auth/actions";
import {singInStyle, styles, NavBarTitle} from './styled';

class HeaderBar extends PureComponent {

  setTitle() {
    const currentRoute = this.props.location.pathname.slice(1);
    switch (currentRoute) {
      case 'poems':
        return <NavBarTitle>Стихи</NavBarTitle>;
      case 'favorite':
        return <NavBarTitle>Избранное</NavBarTitle>;
      case 'settings':
        return <NavBarTitle>Настройки</NavBarTitle>;
      default:
        return <NavBarTitle>Приветствую</NavBarTitle>;
    }
  };

  renderFavorites(classes, signUpRequest, user, ids) {
    return (
        <div className={classes.root}>
          <AppBar position="static">
            <Toolbar style={{minHeight: '48px'}}>
              <Typography variant="title" color="inherit" className={classes.flex}>
                {this.setTitle()}
              </Typography>
              <Button style={singInStyle} color="inherit" onClick={signUpRequest.bind(null, ids)}
                      disabled={user.id ? true : false}>
                {user.id ?
                    <Icon style={{marginRight: '4px'}}>face</Icon> :
                    <Icon style={{marginRight: '4px'}}>fingerprint</Icon>
                }
                {user.id ? user.name : <span>незнакомец</span>}
              </Button>
            </Toolbar>
          </AppBar>
        </div>
    )

  }

  render() {
    const {classes, signUpRequest, user, ids} = this.props;
    return user ?
        this.renderFavorites(classes, signUpRequest, user, ids) :
        <span>Ничего</span>
  }
}

HeaderBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  user: state[moduleName].user,
  location: state.router.location,
  ids: state[moduleName].ids,
  connected: state[moduleName].connected
});

const mapDispatchToProps = ({
  signUpRequest
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(HeaderBar));