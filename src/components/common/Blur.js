import React from 'react';

function Blur(props) {
  return (
     <div style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
       <img src="../img/blur.jpg" alt="Blur" style={{ maxWidth: "100%", height: "auto" }}/>
     </div>
  )
}

export default Blur;