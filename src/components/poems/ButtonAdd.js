import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';
import IconButton from 'material-ui/IconButton';
import Favorite from 'material-ui-icons/FavoriteBorder';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
});

function checkID(uid, addingID) {
  return uid === addingID;
}


function ButtonAdd(props) {
  const {classes, uid, userID, added, addingID} = props;
  const flag = checkID(uid, addingID);
  return (
      <IconButton
          style={{width: "100%", fontSize: "1.2rem", margin: "0 0 -8px 0", borderRadius: "0"}}
          color={added || flag ? "secondary" : "primary"}
          className={classes.button} aria-label="Add to shopping cart"
          onClick={props.addTOFavorite.bind(null, uid, userID)}
      >
        <Favorite/>
        <span style={{fontSize: "0.875rem", marginLeft: "8px"}}>{!added || !!flag ? "В избранное" : "В избранном"}</span>
      </IconButton>
  );
}

ButtonAdd.propTypes = {
  classes: PropTypes.object.isRequired,
  uid: PropTypes.string
};

export default withStyles(styles)(ButtonAdd);