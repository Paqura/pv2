import styled from 'styled-components';

export const PoemWrapper = styled.article`
   width: 100%;
   box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
   background-color: #fff;
   padding: 32px 0 16px 0;
   border: 0;
   &:not(:last-child) {
     margin-bottom: 16px;
   }  
`;

export const PoemText = styled.p`
   font-size: 14px;
   line-height: 1.4;
   white-space: pre-line;
   font-family: 'Open Sans', sans-serif;
`;

export const PoemPicture = styled.img`
   max-width: 100%;
   height: auto;
`;

export const Wrapper = styled.div`
   padding: 0 16px 0 16px;
`;

export const ImgWrapper = styled.span`
   display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
`;
