import React, {Component} from 'react';
import {PoemWrapper, PoemText, PoemPicture, Wrapper, ImgWrapper} from './styled';
import renderHTML from 'react-render-html';
import ButtonAdd from './ButtonAdd';
import LazyLoad from 'react-lazyload';
import Blur from '../common/Blur';

class Poem extends Component {

  findAdded(list) {
    let res = [];
    for (let key in list) {
      if (list.hasOwnProperty(key)) {
        res.push(list[key].id);
      }
    }
    return res;
  }

  createFlag(currID, favIDS) {
    return favIDS.some(it => it === currID);
  }

  renderPoems(poem, ids) {
    const clearText = poem.text.replace(/<br>/g, '\n');
    let flag = this.createFlag(poem.uid, ids);
    return (
        <PoemWrapper key={poem.uid}>
          <Wrapper>
            <h2>{poem.title}</h2>
            <PoemText>
              {renderHTML(clearText)}
            </PoemText>
          </Wrapper>
          <ImgWrapper>
            <LazyLoad placeholder={<Blur/>}  offset={[1200, 0]} debounce={500} height={600}>
              <PoemPicture
                  src={poem.picture}
                  alt="Фото к стишку"
              />
            </LazyLoad>
          </ImgWrapper>
          <ButtonAdd
              userID={this.props.userID}
              uid={poem.uid}
              added={flag}
              addingID={this.props.addingID}
              addTOFavorite={this.props.addTOFavorite}
          />
        </PoemWrapper>
    )
  }

  render() {
    const ids = this.findAdded(this.props.userList);
    return this.props.poems.map(it => this.renderPoems(it, ids))
  }
}

export default Poem;