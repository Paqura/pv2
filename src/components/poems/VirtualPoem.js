import React from 'react';
import {Masonry, AutoSizer} from 'react-virtualized';
import renderHTML from "react-render-html";
import {PoemWrapper, PoemText, PoemPicture} from './styled';

function Poem(props) {
  const {poems} = props;

  function rowRenderer({key, index, style}) {
    const clearText = poems[index].text.replace(/<br>/g, '\n');
    return (
        <div style={style} key={key}>
          <PoemWrapper key={poems[index].uid}>
            <h2>{poems[index].title}</h2>
            <PoemText>
              {renderHTML(clearText)}
            </PoemText>
            <span>
               <PoemPicture src={poems[index].picture} alt="Фото к стишку"/>
            </span>
          </PoemWrapper>
        </div>
    )
  }

  return (
      <div style={{display: "flex", height: "100vh"}}>
        <div style={{flex: '1 1 auto', height: "100%"}}>
          <AutoSizer>
            {({height, width}) => (
                <Masonry
                    width={width}
                    height={height}
                    rowHeight={height}
                    trottle={500}
                    rowCount={poems.length}
                    rowRenderer={rowRenderer}
                />
            )}
          </AutoSizer>
        </div>
      </div>
  )
}

export default Poem;