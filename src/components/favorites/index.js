import React, {Component} from 'react';
import {PoemWrapper, PoemText, PoemPicture, Wrapper} from '../poems/styled';
import renderHTML from 'react-render-html';
import ButtonDelete from './ButtonDelete';


class Favorite extends Component {

  renderFavorites(poem) {
    const clearText = poem.text.replace(/<br>/g, '\n');
    return (
        <PoemWrapper key={poem.id}>
          <Wrapper>
            <h2>{poem.title}</h2>
            <PoemText>
              {renderHTML(clearText)}
            </PoemText>
          </Wrapper>
          <span>
               <PoemPicture src={poem.picture} alt="Фото к стишку"/>
          </span>
          <ButtonDelete deleteFavoritePoem={this.props.deleteFavoritePoem} uid={poem.key} userID={this.props.userID}/>
        </PoemWrapper>
    )
  }

  render() {
    return this.props.favorites ? this.props.favorites.map(it => this.renderFavorites(it)) :
        <span>Пока ничего не добавлено</span>
  }
}

export default Favorite;