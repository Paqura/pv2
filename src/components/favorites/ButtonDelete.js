import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';
import IconButton from 'material-ui/IconButton';
import Delete from 'material-ui-icons/DeleteForever';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit
  },
  input: {
    display: 'none',
  },
});

function ButtonDelete(props) {
  const {classes, uid, userID} = props;
  return (
      <IconButton
          style={{width: "100%", fontSize: "1.2rem", margin: "0 0 -8px 0"}}
          color={"primary"}
          className={classes.button} aria-label="Delete"
          onClick={props.deleteFavoritePoem.bind(null, uid, userID.id)}
      >
        <Delete style={{ height: "32px", width: "32px" }}/>
      </IconButton>
  );
}

ButtonDelete.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ButtonDelete);