import React, {Component} from 'react';
import {Route, Switch} from 'react-router-dom';
import MainPage from './routes/MainPage';
import Poems from './routes/Poems';
import Favorites from './routes/Favorites';
import HeaderBar from './common/NavBar';
import LabelBottomNavigation from './common/BottomBar';
import Reboot from 'material-ui/Reboot';
import ScrollUp from './common/ScrollUp';
import '../config';
import {fetchUsers, getUserSession, signUpRequest} from '../ducks/auth/actions';
import {connect} from 'react-redux';

class Root extends Component {

  componentDidMount() {
    this.props.fetchUsers();
    this.props.getUserSession();
  }

  shouldComponentUpdate(nextProps) {
    if (nextProps.connected !== this.props.connected) {
      return true;
    }
    if (nextProps.ids !== this.props.ids) {
      return true;
    }
    return false

  }

  componentDidUpdate() {
    if (this.props.connected && this.props.ids.length !== 0) {
      this.props.signUpRequest(this.props.ids);
    }
  }


  render() {
    return (
        <div>
          <Reboot>
            <HeaderBar {...this.props}/>
            <main className="main">
              <Switch>
                <Route exact path="/" component={MainPage}/>
                <Route exact path="/poems" component={Poems}/>
                <Route exact path="/favorite" component={Favorites}/>
              </Switch>
            </main>
            <ScrollUp/>
            <LabelBottomNavigation/>
          </Reboot>
        </div>
    )
  }
}

const mapStateToProps = state => ({
  connected: state.auth.connected,
  ids: state.auth.ids
});


export default connect(mapStateToProps, ({fetchUsers, getUserSession, signUpRequest}), null, {pure: false})(Root);