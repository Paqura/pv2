import React, {PureComponent} from 'react';
import Favorite from '../favorites';
import {connect} from 'react-redux';
import Loader from '../common/Loader';
import {favoriteListSelector} from "../../ducks/poems/selectors";
import {fetchFavorite, deleteFavoritePoem} from "../../ducks/favorites/actions";


class Favorites extends React.Component {
  componentDidMount() {
    this.props.fetchFavorite();
  }

  render() {
    const {favoriteList, loading, deleteFavoritePoem, userID} = this.props;
    console.log(favoriteList);
    return (
        <section className="favorites poems">
          {loading ? <Loader/> :
              <Favorite favorites={favoriteList} userID={userID} deleteFavoritePoem={deleteFavoritePoem}/>}
        </section>
    )
  }
}

const mapStateToProps = state => ({
  favoriteList: favoriteListSelector(state),
  loading: state.auth.loading,
  userID: state.auth.user
});

const mapDispatchToProps = ({
  fetchFavorite,
  deleteFavoritePoem
});

export default connect(mapStateToProps, mapDispatchToProps)(Favorites);

