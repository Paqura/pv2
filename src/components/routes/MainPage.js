import React, { Component } from 'react';
import styled from 'styled-components';
import CommonPage from '../main';

const S = styled.span`
@keyframes fall {
    from {
      transform: translateY(-1000px);
    }
    to {
    transform: translateY(0);
    }
  }
  display: flex;
  width: 100%;
  height: 100%;
  animation-name: fall;
  animation-duration: 1200ms;
  animation-timing-function: ease;
  z-index: -1;
  position: relative;
  
`;

const Pic = styled.img`
   max-width: 100%;
   height: auto;
   object-fit: contain;
`;


class MainPage extends Component {
  render() {
    return (
      <div>
        <CommonPage/>
      </div>
    )
  }
}

export default MainPage;