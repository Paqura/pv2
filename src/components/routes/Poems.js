import React, {PureComponent} from 'react';
import Poem from '../poems';
import {connect} from 'react-redux';
import {moduleName} from '../../ducks/poems';
import {fetchPoems, fetchLazyPoems} from '../../ducks/poems/actions';
import { addTOFavorite } from '../../ducks/favorites/actions';
import {poemsListSelector} from '../../ducks/poems/selectors';
import Loader from '../common/Loader';

class Poems extends PureComponent {
  componentDidMount() {
    this.props.fetchPoems();
    window.addEventListener('touchend', this.handleScroll.bind(this));
  }
  componentWillUnmount() {
    window.removeEventListener('touchend', this.handleScroll);
  }

  handleScroll() {
    let pageHeight = document.body.scrollHeight;
    let currentPosition = window.pageYOffset + window.innerHeight;

    if (pageHeight - currentPosition < 1000) {
      this.props.fetchLazyPoems();
    }
  }

  render() {
    const {loading, loaded, poemsList, addTOFavorite, userID, userList, addingID} = this.props;

    return (
        <section className="poems">
          {loading ? <Loader/> : <Poem poems={poemsList} userList={userList} userID={userID} addTOFavorite={addTOFavorite} addingID={addingID}/>}
          {loaded ? null : <Loader/>}
        </section>
    )
  }
}

const mapStateToProps = state => ({
  poemsList: poemsListSelector(state),
  loading: state[moduleName].loading,
  loaded: state[moduleName].loaded,
  userID: state.auth.user.id,
  userList: state.auth.user.favorites,
  addingID: state.auth.adding
});

const mapDispatchToProps = ({
  fetchPoems,
  fetchLazyPoems,
  addTOFavorite
});

export default connect(mapStateToProps, mapDispatchToProps)(Poems);