module.exports = function () {
  return {
    files: [
      'src/**/*.js'
    ],

    tests: [
      'src/**/*.test.js'
    ]
  };
};